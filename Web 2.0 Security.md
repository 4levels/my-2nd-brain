
# Web 2.0 Security
## aka
## How to "OAuth2"

---
## What is web 2.0 and why does it matter?

The way information is shared on the Internet has changed drastically in the last decades.
Whole new domains and standards have emerged tightly related to the rise of JavaScript as
all-purpose programming language.  Some related historical facts:
- Web pages can still be hosted as HTML complete files on a computer.
- Programming languages designed to manipulate HTML emerged quickly, allowing 'dynamic pages',
  thereby integrating with databases and other services, so the server can generate different HTML based on context.
- JavaScript started as a browser-embedded language to manipulate HTML on the device viewing it (client)
  instead of the device serving it (server)
- Web 2.0 indicates the change from HTML being generated on the server to HTML being generated
  on the client (with JavaScript)

---
## Buzzwords and new paradigms

The shift towards JavaScript brought new trends, new buzzwords, new paradigms..

- "Frontend" got a whole new meaning as umbrella term
- "Backend" became more refined towards servers and data processing
- "UX/UI" emerged and became a whole new domain
- JavaScript became a fast-paced ecosystem
- New trends like 'responsive' and 'reactive' design emerged
- ReactJS and AngularJS paved the path for the numerous frontend frameworks that now happily coexist,
  eg. VueJS, Svelte, ..
- Almost all frontend frameworks need NodeJS as development platform
- Frontend Frameworks are highly complicated but extremely well integrated, making it very easy to start,
  but very difficult to understand, let alone debug

---
## Web Security

Security is the driving force behind a lot, if not all, evolution in the Web domain.  No matter what the
server is doing, it has to adhere to the security standards to become part of the public internet.
Security standards are enforced on every party involved, going from Operating System to Web Browsers,
integrated in the very core of the internet, HTTP and TCP/IP.

Encryption and credentials are key components that allow complex processes like SSL, anonymous validation
or stateless, token-based sessions.  OAuth2 was developed as new authorization standard to handle the quick
rise of applications running client-side.  Mobile apps and Single Page Applications are, just like HTML,
public by design since the complete source is downloaded to the client device up-front.

---
## Authorization and Authentication

Both Authorization and Authentication are often used interchangeably, but they are not at all the same!

## Authorization = Access Management

Regulating access to a resource usually involves the concept of roles, optionally regulated via groups
and/or permissions.  OAuth was specifically designed for this: Authorize access to confidential resources. 

## Authentication = Identity Management 

Identifying users is a core concept of Access Management, but a vastly different regulated domain.
Identity Management is far more stricter controlled world wide.  Privacy is an important part of this and
comes with it's own slew of challenges and restrictions.

---
## Identity and Access Management - IAM

Umbrella term for regulating both Authorization and Authentication, that fully covers the whole process
of allowing regulated access to restricted resources.

### SAML and OpenID Connect

[SAML](https://en.wikipedia.org/wiki/Security_Assertion_Markup_Language) is an specification mainly designed
around the use case of Single Sign On.  It describes the various parties and components involved in great detail.
It's implemented in XML and commonly used over SOAP requests.

[OpenID Connect](https://openid.net/developers/how-connect-works/) was developed as authentication extension
of the OAuth2 framework, using an Identity Token. 

---
## OAuth2  - Introduction

OAuth2 is defined in RFC 6749 and uses a lot of specific vocabulary.  The following slides will cover each
of the below topics in more detail.

- Confidentiality, Public vs Confidential Client
- Scope, Grant, Resource, Client, ..
- AccessToken, RefreshToken, AuthCode, IdentityToken, ..
- Auth Flow
- Auth Provider and Multi-Auth

OAuth2 RFC's:
[6749 (OAuth2)](https://datatracker.ietf.org/doc/html/rfc6749),
[6750 (Bearer Token)](https://datatracker.ietf.org/doc/html/rfc6750),
[7519 (JWT)](https://datatracker.ietf.org/doc/html/rfc7519),
[7636 (PKCS)](https://datatracker.ietf.org/doc/html/rfc7636)

---
## OAuth2 - Confidentiality

With the advent of Web 2.0, where end-user devices download and run a JavaScript application, Confidentiality
became a very strong separating factor: any code running on a device that is not managed by the resource owner
or client, has to be considered Non-Confidential.

OAuth2 uses the concepts of [Public Client and Confidential Client](https://oauth.net/2/client-types/)
to describe this core difference.

---
## OAuth2 - Confidentiality - Public Client

Assigned to any application that runs on public hardware (read: smartphone, laptop, desktop, .. not controlled
by the organization).  Automatically applies to Mobile Apps and JavaScript browser applications.

Technically this forbids any confidential data to exist in the code as it is considered relatively easy to
reverse engineer Mobile and JavaScript applications.  This makes an external API a requirement in order to
retrieve any confidential information.

---
## OAuth2 - Confidentiality - Confidential Client

Assumed by any application that runs on controlled hardware (read: server, managed desktop, managed smartphone, .. ).
Allows the code to work with and use credentials as the hardware running the code is considered secure.

### Credentials store and distribution

Secrets are not supposed to exist anywhere except for the hardware running the code.  Best practices suggest
storing the credentials in memory only and having the software use them via an identifier (eg. a file path
to a memory disk) so the only thing that can ever get exposed in code is the name of the credential, not it's value.
Most providers offer a credential storage and distribution solution to meet these requirements.

---
## OAuth2 - Resource, Scope, Grant, Client, User

- Resource or "*what can I **share***"
  Describes the process of a Resource owner defining what information to expose.
- Scope or "*what can I **access***"
  Considered as the "role/permission" part of Access Management.
- Grant or "***how** do I get or give access*"
  Limits the ways of getting / giving access into very specific ways.
- Client or "*who do I manage*"
  Intermediary that validates user access.
- User or "*who am I*"
  Identifier of an application or user accessing the information.

--- 
## OAuth2 - Resource

Regulating access to resources is the core concept of OAuth2.  A Resource can be considered a data source
that needs access regulation.  The Resource Owner is responsible for the particular data source and defines
how it can be accessed.  The assignment of roles, commonly involving groups and permissions, is a core
responsibility of the Resource Owner.

---
## OAuth2 - Scope

Scopes are defining what kind of access a user can have.  As the meaning of the word, a scope covers the need
for a use case or it's components.

Scopes can freely be defined, some common approaches:
- Sharing your personal information (eg. email, name) via a third party identity provider like CSAM.
- Allowing access to a specific resource.
- Enabling a specific use-case.

Note that how scopes are defined and used is not strictly regulated as this is up to the implementation.

---
## OAuth2 - Grant

OAuth2 limits the ways access can be granted via the use of various specific Grant Types.  This is a hard
requirement and strictly regulated via various [RFC's](https://en.wikipedia.org/wiki/Request_for_Comments).

- [**Authorization Code Grant**](https://oauth.net/2/grant-types/authorization-code/)
  Single-use code, used by device-to-device communication.
- [**PKCE**](https://oauth.net/2/pkce/)
  Extra hardening concept to prevent CSRF and code injection attacks.
- [**Refresh Grant**](https://oauth.net/2/grant-types/refresh-token/)
  Allows refreshing an expired token, enabling stateless sessions.
- [**Client Credentials Grant**](https://oauth.net/2/grant-types/client-credentials/)
  Uses a Client ID and secret to validate an Authentication request.
- [**Password Grant**](https://oauth.net/2/grant-types/password/)
  Allows validating User credentials, considered legacy in OAuth2.
- [**Implicit Grant**](https://oauth.net/2/grant-types/implicit/)
  Grant type for backward compatibility, considered legacy in OAuth2.

---
## OAuth2 - Client

Intermediary governing resources, referenced with any Authorization request.  Considered as a trusted party
by the various providers and allowing authenticated communication.

Uses the Client Credentials Grant to pass a known client id and secret to the provider alongside the
authorization request.

---
## OAuth2 - User

Unique identifier for an application or user interacting with the resource.  Anonymous by design and
authenticated by an Identity Provider.  Cannot contain any Personal Identifiable Information.  Used by the
Access Token and Authorization Code to link an Identity reference to a scope, authorizing the User anonymously.

---
## OAuth2 - Tokens

Tokens are the core of the credential mechanism and exist of cryptographic generated strings.  They can only
be validated by the instance that generated them via the SSL private and public key mechanism, allowing the
highest level of security.  Various types of tokens exist to allow the different ways of integrating authorization.
In a sense OAuth2 can be considered as a token minting service.

The following token types are defined:
- AccessToken
- RefreshToken
- AuthCode
- IdentityToken - OpenID Connect

---
## OAuth2  - Access Token

Considered as "temporary hashed passwords", passed as string value along with the request.  A common pattern is
to use the HTTP Authorization header as Bearer token.  They are constraint in usage as they can only transmit
anonymous scope assignments and are only allowed when accessing resources requiring authorization.  Short lived
by design with a common Time To Live of 1 hour.

Sent with every request to a resource from a Public Client.  Can be persisted on the client for repeated use.
Validated on each use by the Authorization service.

---
## OAuth2 - Refresh Token

Sent along with the Access Token response upon successful authorization and can be used only once to refresh
authorization without forcing a new authentication.  Generally more long-lived with common Time To Live values
ranging from a few days to a couple of months.

Provides the largest attack vector especially when used with Single Page Applications.  A common pattern to limit
Refresh Token re-use is to immediately invalidate all tokens related to the refresh token, effectively forcing
re-authentication.

---
## OAuth2  - Authorization Code

Sent along with a Client ID and secret to be exchanged for an Access Token.
Can only be used by Confidential Clients as the client secret cannot be exposed to a Public Client by design.
Supposed to be consumed as part of an authorization request, exchanging the code for an Access Token.

Can have a very short Time To Live of for instance 10 minutes if used in a One Time Login process.

---
## OAuth2 - Identity Token - OpenID Connect

Identity driven JWT token, extending the OAuth2 token framework via the OpenID Connect protocol.  Issued by an
OIDC Provider, allowing seamless integration with multiple relying parties to authenticate a user.  Contains
signed user information and is meant to be consumed by the client application, in contrast to Access Tokens
which are validated by the resource owner.  Defines a set of common properties containing typical identifying
information like issuing server details and other common identifier fields, abbreviated to reduce the token size.

---
## OAuth2 - Authorization Flow

The complete request flow is a convoluted process involving multiple parties to ensure maximum separation of
concerns whilst maintaining cryptographic integrity.  According to the OAuth2 specifications, the following
participants are actively processing parts of the authentication flow:

- **Client**: the Public Client application running on the end-user's device
- **Authorization Provider**: instance regulating access
- **Authentication Provider**: instance regulating identification
- **Resource**: instance providing restricted data

---
## OAuth2 - Flow - Authorized request

```mermaid
sequenceDiagram
  autonumber
  participant PC as Public Client
  participant RS as Resource Provider
  participant AM as Authorization Provider

  PC ->> RS: get with accessToken
  RS ->> AM: validate accessToken
  AM -->> RS: valid
  RS ->> RS: handle scope
  RS -->> PC: response
```

---
## OAuth2 - Flow - Authorization request

```mermaid
sequenceDiagram
  autonumber
  participant PC as Public Client
  participant RS as Resource Provider
  participant AM as Authorization Provider
  participant IM as Identity Provider

  PC ->> RS: get without token
  RS ->> AM: request access
  AM -->> PC: provide identity
  PC ->> IM: identify
  IM ->> IM: authenticate
  IM ->> IM: grant scope access
  IM -->> AM: identity
  AM ->> AM: authorize
  AM -->> PC: accessToken
  PC ->> RS: get with accessToken
```

---

## OAuth2 - Flow - Refresh Token

```mermaid
sequenceDiagram
  autonumber
  participant PC as Public Client
  participant RS as Resource Provider
  participant AM as Authorization Provider

  PC ->> RS: get with accessToken
  RS ->> AM: validate accessToken
  AM -->> PC: token expired
  PC ->> AM: refresh token
  AM -->> PC: new accessToken
  PC ->> RS: get with new accessToken
```

---
## OAuth2 - Auth Provider and Multi-Auth

The concept of an Auth provider describes integrating multiple authorization and authentication providers
through a single endpoint.  Since the endpoint is considered a Confidential Client, the underlying code can
invoke different providers based on business logic, operational constraints or legal constraints.

Typically Multi-Auth providers are using various clients (with their unique id & secrets) to allow both access
management and identity management.  By implementing it's own token infrastructure, a Multi-Auth provider can
limit the number of confidential requests.  This prevents external providers becoming a hard dependency since
they only are needed for identification purposes (Authentication).

---
## Questions and Answers

- who has used OAuth2 before?
- how often do you think you use OAuth2 on a daily basis?

---

## References

- [OAuth2 Authorization Servers](https://developer.okta.com/docs/concepts/auth-servers/) excellent documentation of the Authorization Server concept
- [OAuth2 and OpenID Connect overview](https://developer.okta.com/docs/concepts/oauth-openid/) Okta developers tech resources on OpenID Connect
- [OAuth2 Security Best Current Practice](https://www.ietf.org/archive/id/draft-ietf-oauth-security-topics-24.html) RFC specification is currently in draft, updating RFC 6749, 6750 and 6819 if approved.
- [OAuth2 Refresh Token Best practices](https://stateful.com/blog/oauth-refresh-token-best-practices) great write-up of common best practices related to Refresh Tokens.
