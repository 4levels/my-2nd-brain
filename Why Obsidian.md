The way Obsidian can interact with a collection of markdown files is extremely well designed.  Not only does it allow referencing anything via it's internal reference and link tracking, it has beautiful visualizations and tools built-in.  Need a slideshow? Enable the core plugin.  Need some custom styling? Add a snippet.

### Markdown support and live preview.

Obsidian renders markdown whilst you're typing it, providing instant formatting feedback.  Syntax highlighting happens whilst typing in any code styled block, defined by 3 back-tick characters and the language name, eg. `sql` for a block of SQL code.  Keep the back-tick characters and the language name on a separated line for code blocks.

View the source of this document to see the syntax of the examples below.

| Simple | Tables | Made | Easy |
| :----: | ------ | ---: | ---- |
| wow    | nice   | ok   | 8    |
| huh    | no way | rip  | 8.5  |

[MermaidJS](https://mermaid.js.org/) is fully supported out of the box, next to syntax highlighting for **almost any common programming format and language**.


```mermaid
flowchart TD
  A>Look] --- B((Mom))
  B --> C("SVG with JS")
  C --> A
```
```
flowchart TD
  A>Look] --- B((Mom))
  B --> C("SVG with JS")
  C --> A
```

```sql
SELECT "user"."key", "user".username
FROM database.users as "user"
```

```json
{ "method": "POST" }
```

```xml
<reference>
  <docnr reference="TRT8">
  IXF000021651
  </docnr>
</reference>
```
