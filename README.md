# My 2nd Brain

I created this repo after discovering [Obsidian](https://obsidian.md) as technical note taking app.
Because it has so many great features for documenting all whilst using markdown, I'm currently so
enthusiastic that I just had to make a public git repo for this.  I've been using Obsidian for work
related documentation lately and the more content you add, the more amazing it becomes to
understand bigger topics.

*I've picked up the phrase "2nd brain" (whilst referring to Obsidian as productivity tool)
from various online sources.  After seeing Obsidian turn a complex documentation project
into an understandable and conveyable data source it still feels worthy of the title "second brain",
hence the name of this project.*

*Everything is still very early and work in progress.*

The ultimate goal is to create a bunch of tech related topics together, collecting knowledge I
find to keep track of the ever growing total picture in my head.  Since I have most, if not all,
of my knowledge thanks to OpenSource projects, I'm considering this as an open project as well.
Besides, there's not really much to 'close' when using plain text markdown to develop your own
personal opinions.
