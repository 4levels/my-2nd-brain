I've grouped some personal history and various information below.

- I've always been very curious about how things work.
- I still haven't reached the understanding of how programming instructions translate to electric signals.
- I've never had to study for IT as it was a major personal interest early on.
- If it wasn't for both Open and Closed Source projects I wouldn't be where I am right now.

#### Tech Influencers I currently watch:
- The Primeagen - Netflix developer, hilarious loud-mouth tech guru
- CodeAesthetics - Code, in it's most beautiful form and function
- Kevin Powell - CSS Guru and artisan
- Rob Braxman - Security and Privacy specialist
- Dave's Garage - All things Windows, great story teller
- and many, many, many, more..

#### Companies / Products I've liked for a long time:
- CloudFlare
- Gitlab / Bitbucket / Github
- OpenProvider
- Vultr
- Google

#### Windows history:
- I've seen 3.11, but started with 95
- CAD / 3D enthousiast before dongles even existed
- Early windows networking and dial-up
- Browser wars were really bad
- AOE
- Grmbl#$ printers

#### Linux history:
- SuSE linux (1999) - Kameleon vibes
- Gentoo linux (2000) - *"please mom, one more emerge"*
- Debian linux (2001) - *"Ubuntu roots"*
- NixOS (2012) - not just another distro, the more you know..

#### WebDev:
- started with PHP 4.3, Apache and MySQL
- messed with Flash banners when they were still hot
- using home-made PHP template engine before that was even a thing, thx @
- early Symfony enthusiast and heavily influenced by their excellent "Gentle Introduction"
- window.confirm() -> jQuery -> React -> Vue -> who can keep up these days
- developed through (X)HTML and web 2.0
  *"Frontend is a thing now?"* and *"isn't UI/UX just how you'd expect it to be?"*
- REST + GraphQL + OAuth2 with PHP Lumen and React Native frontend experimentations
- Cloudflare Edge and KV amateur

#### General:
- "Standards rock"
